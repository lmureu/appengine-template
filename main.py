from flask import Flask, render_template, request, redirect, url_for
import item_datastore

app = Flask(__name__)


@app.route('/')
def hello_world():
    return render_template("main.html", title="A custom title",
                           message="Mama tua bagassa")


@app.route('/new', methods=["GET", "POST"])
def new_item():
    if request.method == "POST":
        item_datastore.create_item(request.form['title'],
                                   request.form['message'])
        return redirect(url_for('list_items'))
    else:
        return render_template("new_item.html")


@app.route('/list')
def list_items():
    items = item_datastore.get_items()
    return render_template("list.html", items=items)


if __name__ == '__main__':  # pragma: no cover
    app.run()
