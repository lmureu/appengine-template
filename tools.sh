#!/usr/bin/env bash

# Global environment variables
export GOOGLE_APPLICATION_CREDENTIALS="credentials.json"
export CLOUDSDK_CORE_PROJECT="testbackend-240516"

# Local variables
DATASTORE_EMULATOR_ADDRESS="localhost:8258"


# Local functions
usage() {
    cat <<EOL
Syntax: $0 action

Where action can be:
    run                     run the application locally (using the Cloud's datastore)
    run-local               run the application locally (using emulated datastore)
    db                      run the data-store emulator
    test                    run the unit tests (using emulated datastore)
    coverage                perform coverage analysis over tests (including branches) (using emulated datastore)
    coverage-clean          remove coverage data
    deploy                  [Not Implemented] Deploy the application to Google App Engine
EOL
}

db-init() {
    $( gcloud beta emulators datastore env-init )
}

db-deinit() {
    $( gcloud beta emulators datastore env-unset )
}

[[ $# -lt 1 ]] && {
    usage
    exit
}

case $1 in
    run)
        echo "Running using GOOGLE datastore"
        python main.py
        ;;

    run-local)
        echo "Running using LOCAL datastore"
        db-init
        python main.py
        db-deinit
        ;;

    db)
        echo "Starting datastore emulator"
        gcloud beta emulators datastore start --host-port=${DATASTORE_EMULATOR_ADDRESS} --no-store-on-disk
        ;;

    test)
        echo "Starting tests..."
        db-init
        python -m unittest discover -s tests -p "*.py"
        db-deinit
        ;;

    coverage)
        echo "Starting coverage..."
        db-init
        coverage run --omit 'venv/*,tests/*'  --branch -m unittest discover -s tests -p "*.py"
        coverage report
        db-deinit
        ;;

    coverage-clean)
        echo "Cleaning after coverage..."
        coverage erase
        rm -rf htmlcov
        ;;

    deploy)
        echo "Unimplemented function"
        exit 4
        ;;

    *)
        echo "Unrecognized action: $1"
        usage
        exit 1
        ;;
esac
