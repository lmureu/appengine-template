import os
import sys
import unittest
from flask_testing import TestCase

sys.path.insert(1, os.path.realpath("."))
import main


class Main(TestCase):
    def create_app(self):
        return main.app

    def test_hello_world(self):
        response = self.client.get('/')
        self.assertStatus(response, 200)
        # self.assertTemplateUsed(response, "main.html")

    def test_new_item_form(self):
        response = self.client.get('/new')
        # self.assertTemplateUsed(response, "new_item.html")
        self.assertStatus(response, 200)

    def test_new_item_post(self):
        response = self.client.post('/new',
                                    data=dict(title="title", message="message"))
        # self.assertTemplateUsed(response, "list.html")
        # self.assertStatus(response, 200)
        self.assertRedirects(response, "/list")

    def test_list_items(self):
        response = self.client.get('list')
        self.assertStatus(response, 200)


if __name__ == '__main__':
    unittest.main()
