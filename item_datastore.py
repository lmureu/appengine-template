from google.cloud import datastore

_datastore_client = datastore.Client()


def create_item(title, message):
    key = _datastore_client.key("Item")

    item = datastore.Entity(key)
    item['title'] = title
    item['message'] = message

    _datastore_client.put(item)
    return _datastore_client.key


def get_items():
    query = _datastore_client.query(kind="Item")
    return query.fetch()
